Gauge
=====

This API is build to generate and manage gauge via a cool API ;)


[A11y](https://duckduckgo.com/?q=a11y+define&ia=definition)
----

The generate gauge repect the ARIA norm.
The label of the bars are only for screen readers.


[API](https://en.wikipedia.org/wiki/Application_programming_interface)
---

### method

#### Generate a gauge

```javascript
// generate a gauge with the default label
var myGauge = new Gauge();

// generate a gauge with 2 bars and with the default label
var myDualGauge = new Gauge(2);

// generate a gauge with 2 bars and with custom labels
var myDualGaugewithLabel = new Gauge(2, [
	'labelForBar1',
	'labelForBar2'
]);
```

#### Set a new start point

```javascript
// set the start of the first bar at 23.5%
myDualGauge.setStart(23.5);

// set the start of the first bar at 9%
myDualGauge.setStart(9, 0);

// set the start of the second bar at 34%
myDualGauge.setStart(34, 1);
```

#### Get the start point

```javascript
// get the start of the first bar
myDualGauge.getStart();

// get the start of the first bar
myDualGauge.getStart(0);

// get the start of the second bar
myDualGauge.getStart(1);
```


#### Set a new progression

```javascript
// set the progress of the first bar at 73.1%
myDualGauge.setProgress(73.1);

// set the progress of the first bar at 44.9%
myDualGauge.setProgress(44.9, 0);

// set the progress of the second bar at 32.4%
myDualGauge.setProgress(32.4, 1);
```


#### Get a new progression

```javascript
// get the progress of the first bar
myDualGauge.getProgress();

// get the progress of the first bar
myDualGauge.getProgress(0);

// get the progress of the second bar
myDualGauge.getProgress(1);
```


#### Set a new label

```javascript
// Set the label 'myLabel' to the first bar
myDualGauge.setLabel('myLabel');

// Set the label 'myNewLabel' to the first bar
myDualGauge.setLabel('myNewLabel', 0);

// Set the label 'myLabel' to the second bar
myDualGauge.setLabel('myLabel', 1);

// Set the default label to the first bar
myDualGauge.setLabel();

// Set the default label to the first bar
myDualGauge.setLabel(null, 0);

// Set the default label to the second bar
myDualGauge.setLabel(null, 1);
```


#### Get a new label

```javascript
// Get the label of the first bar
myDualGauge.getLabel();

// Get the label of the first bar
myDualGauge.getLabel(0);

// Get the label of the second bar
myDualGauge.getLabel(1);
```

### Events

The events are fired when a new value is request. It's fired with the mouse.
When the user start to request a new value (event based on the `mousedown` event) the `requestValueStart` event will be trigger.

The user will move his mouse. This action will fire a new event : the `requestValue` event.

At the end, when the user think that he choose the good value, he will loosen his left click, the `requestValueStop` event will fired.

This events are made with the `CustomEvent` object. You should use the polyfill provided with this programm.


#### requestValueStart

You can't retrieve the new value with this event.

```javascript
myGauge.addEventListner('requestValueStart', function (event) {
	console.log('Start to requesting a new value...');
});
```


#### requestValue

You can't retrieve the new value with this event.

```javascript
myGauge.addEventListner('requestValue', function (event) {

	var newValue = event.detail.newValue;

	console.log('Requesting a new value...');
	console.log('And this new value is ' + newValue);
});
```


#### requestValueStop

You can't retrieve the new value with this event.

```javascript
myGauge.addEventListner('requestValueStop', function (event) {
	console.log('Stop requesting a new value...');
});
```


